import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PicofthedayComponent } from './picoftheday.component';

const routes: Routes = [{ path: '', component: PicofthedayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PicofthedayRoutingModule { }
