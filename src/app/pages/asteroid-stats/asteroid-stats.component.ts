import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/service/web.service';

@Component({
  selector: 'app-asteroid-stats',
  templateUrl: './asteroid-stats.component.html',
  styleUrls: ['./asteroid-stats.component.scss']
})
export class AsteroidStatsComponent implements OnInit {

  constructor(public ws: WebService) { }

  ngOnInit() {
    this.ws.getAsteroidStats().subscribe(res => {
      console.log(res);
      if (res.callback = "success") {
        console.log(res.contextWrites.to);
      } else {

      }
    }, error => {
      console.log(error);
    })
  }

}
