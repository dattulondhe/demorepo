import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsteroidStatsRoutingModule } from './asteroid-stats-routing.module';
import { AsteroidStatsComponent } from './asteroid-stats.component';


@NgModule({
  declarations: [AsteroidStatsComponent],
  imports: [
    CommonModule,
    AsteroidStatsRoutingModule
  ]
})
export class AsteroidStatsModule { }
