import { Injectable, NgModule } from '@angular/core';
import { HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterceptApiService {
  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Clone the request and replace the original headers with
    // cloned headers, updated with the Content-Type.
    const authReq = req.clone({
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-rapidapi-key': 'U2xXn1eRZxmsh4o9vBTaYth4Pcrap1XUm42jsnGIgEADbbCcD3'
      })
    });

    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
};




